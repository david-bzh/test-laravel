# Test Laravel

Test d’évaluation backend-end


### Prerequisites

Server [Requirements](https://laravel.com/docs/8.x/deployment#server-requirements)

* PHP >= 7.3
* BCMath PHP Extension
* Ctype PHP Extension
* Fileinfo PHP Extension
* JSON PHP Extension
* Mbstring PHP Extension
* OpenSSL PHP Extension
* PDO PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension

## Getting Started

```
git clone https://gitlab.com/david-bzh/test-laravel.git
```

### Installing

```
composer install
yarn install && yarn run dev
```

### Setup database
```
composer run setup-database 
```
For Sqlite ( .env )
```
DB_CONNECTION=sqlite
```

### Admin

* Url : http://127.0.0.1:8000/dashboard
* Email : admin@example.com
* Password : password

## Built With

* [Laravel 8](https://github.com/laravel/laravel) - The web framework used
* [Jetstream](https://github.com/laravel/jetstream) - Dependency Management
* [livewire](https://github.com/livewire/livewire) - Dependency Management



## Authors

* **David Baller** -

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details


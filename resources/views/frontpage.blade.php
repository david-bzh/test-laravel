<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        <style>@import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);</style>

        @livewireStyles

        <!-- Scripts -->
        <script src="{{ mix('js/app.js') }}" defer></script>

    </head>
    <body class="antialiased">

        <nav class="bg-white flex items-center justify-between flex-wrap  ">
            <div id="main-nav" class="w-full flex-grow lg:flex items-center lg:w-auto hidden  ">
                <div class="text-sm lg:flex-grow m-3 animated jackinthebox xl:mx-8">
                    <a href="/"
                        class="block lg:inline-block text-md font-bold  text-orange-500  sm:hover:border-indigo-400  hover:text-orange-500 mx-2 focus:text-blue-500  p-1 hover:bg-gray-300 sm:hover:bg-transparent rounded-lg">
                        HOME
                    </a>
                </div>
            </div>
        </nav>

        <main class="main  bg-yellow-300" role="main" >
            @livewire('frontpage')
        </main>

        @livewireScripts
    </body>
</html>

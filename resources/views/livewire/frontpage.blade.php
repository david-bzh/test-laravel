<div class="frontpage">
    <div class="container my-12 mt-0 mb-0 mx-auto px-4 md:px-12">
        <div class="flex flex-wrap -mx-1 lg:-mx-4 pt-4">
            @if ($data->count())
                @foreach ($data as $item)
                    <div class="my-1 px-1 w-full md:w-1/2 lg:my-4 lg:px-4 lg:w-1/3">
                        <article class="overflow-hidden rounded-lg shadow-lg  bg-white">
                                <img alt="Placeholder" class="block h-auto w-full" src="{{$item->image}}" loading="lazy">
                                <div class="flex items-center justify-between leading-tight p-2 md:p-4">
                                    <h1 class="text-lg">
                                        <a class="no-underline hover:underline text-black" href="{{ URL::to('/'.$item->slug)}}">
                                            {{ $item->title }}
                                        </a>
                                        <div class="block mt-5">
                                            <span class="text-2xl leading-none align-baseline">$</span>
                                            <span class="font-bold text-5xl leading-none align-baseline">{{rand(1,50)}}</span>
                                            <span class="text-2xl leading-none align-baseline">.{{rand(90,99)}}</span>
                                        </div>
                                    </h1>
                                </div>
                                <div class="flex items-center justify-between leading-none p-2 md:p-4 mb-3">
                                    <div class="block align-bottom">
                                        <a  href="{{ URL::to('/'.$item->slug)}}" class="bg-yellow-300 opacity-75 hover:opacity-100 text-yellow-900 hover:text-gray-900 rounded-full px-10 py-2 font-semibold"><i class="mdi mdi-cart"></i></a>
                                    </div>
                                </div>
                        </article>
                    </div>
                @endforeach

            @else
                <tr>
                    <td class="px-6 py-4 text-sm whitespace-no-wrap" colspan="4">No Results Found</td>
                </tr>
            @endif
        </div>
    </div>
    <div class="py-4 px-2 lg:mx-4 xl:mx-12 ">
        <div class="flex items-center justify-between flex-wrap  ">
            <div id="main-nav" class="w-full flex-grow lg:flex items-center lg:w-auto hidden  ">
                <div class="text-sm lg:flex-grow mt-2 animated jackinthebox xl:mx-8">
                    {{ $data->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

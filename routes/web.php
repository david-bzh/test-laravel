<?php

use App\Models\Product;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => [
    'auth:sanctum',
    'verified',
]], function () {
    Route::get('/dashboard', function () {
        return view('admin.products');
    })->name('dashboard');
});

Route::get('/', function () {
    return view('frontpage');
});

Route::get('/{Product:slug}', function ($slug) {
    return view('frontproducts',[
        'product' => Product::where('slug', $slug)->first()
    ]);
});

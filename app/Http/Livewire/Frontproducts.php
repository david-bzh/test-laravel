<?php

namespace App\Http\Livewire;

use Livewire\Component;


class Frontproducts extends Component
{
    public $product;
    public $title;
    public $image;
    public $content;

    /**
     * The livewire mount function.
     *
     * @param  mixed $urlslug
     * @return void
     */
    public function mount($product)
    {
        $this->retrieveContent($product);
    }

    /**
     * Retrieves the content of the page.
     *
     * @param  mixed $urlslug
     * @return void
     */
    public function retrieveContent($product)
    {

        // Get the page according to the slug value
        if (empty($product)) {

            $this->title    = 'Page not found';
            $this->content  = 'The server can not find requested resource';
            $this->image    = 'https://www.elegantthemes.com/blog/wp-content/uploads/2020/02/000-404.png';
        } else{

            $this->title   = $product->title;
            $this->image   = $product->image;
            $this->content = $product->content;
        }
    }

   /**
     * The livewire render function.
     *
     * @return void
     */
    public function render()
    {
        return view('livewire.frontproducts');
    }
}

<?php

namespace App\Http\Livewire;

use App\Models\Product;
use Livewire\Component;
use Livewire\WithPagination;


class Frontpage extends Component
{

    use WithPagination;

    /**
     * The read function.
     *
     * @return void
     */
    public function read()
    {
        return Product::paginate(5);
    }

    /**
     * The livewire render function.
     *
     * @return void
     */
    public function render()
    {
        return view('livewire.frontpage', [
            'data' => $this->read(),
        ]);
    }
}

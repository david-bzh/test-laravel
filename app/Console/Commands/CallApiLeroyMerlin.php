<?php

namespace App\Console\Commands;

use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Whoops\Run;

class CallApiLeroyMerlin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:call:api:leroy-merlin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Call api Leroy Merlin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $apiKey = 'vx64AonXBZVIIDkvhZHskyQLEN15iLk2';

    /**
     * The console command description.
     *
     * @var object
     */
    protected $tableProducts;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->tableProducts = DB::table('products');

        /** @var $response Http */
        $products = $this->callApiProducts('/nomenclatures/web/3000018/products');

        if ( !empty($products['data']) ){

            $this->tableProducts->delete();

            $this->info('table clear and start update Table...');

            foreach ($products['data'] as $product ) {
                $this->updateTableProducts($product['id']);
            }

            $this->info('end update Table...');
        }
    }

     /**
     * Update table Products with new data.
     *
     * @var id $productId
     * @return void
     */
    private function updateTableProducts($productId){

        $product = $this->callApiProducts('/products/' . $productId);

        if ( !empty( $product ) ) {

            $description = $this->getDescriptionProduit($productId);
            $image = $this->getImageProduit($productId);

            $this->info('insert... ' . $product['label']);

            $this->tableProducts->insert([
                'title' => $product['label'],
                'slug' => Str::slug($product['label'] . $productId),
                'image' => $image,
                'content' => $description,
            ]);
        }
    }

    /**
     * Returns an description of the product.
     *
     * @var id $productId
     * @return string
     */
    private function getDescriptionProduit($productId){

        $characteristics = $this->callApiProducts('/products/'. $productId .'/characteristics');

        $description  = '<div>';
        if ( isset( $characteristics['data']) && !empty($characteristics['data']) ){

            $count = 0;
            foreach ($characteristics['data'] as $characteristics ) {

                if( !empty($characteristics['values']) )
                    $description .= '<p>' . $characteristics['label']. ' : '. $characteristics['values'][0] .'</p><br>';

                if ($count === 4){
                    break;
                }
                $count++;
            }
            $description .= '</div>';
        }

        if( strlen( $description ) ===  11 )
            $description = '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>';

        return $description;
    }

    /**
     * Returns an image of the product.
     *
     * @var id $productId
     * @return string
     */
    private function getImageProduit($productId){

        $media = $this->callApiProducts('/products/'. $productId .'/media');

        if( ! empty( $media['data'] ) && isset( $media['data'][0]['url'] ) )
            $media = $media['data'][0]['url'];
        else
            $media ='https://picsum.photos/600/400/?random';

        return  $media;
    }

    /**
     * Call Api Produit Lery Merlin.
     *
     * @var string $request
     * @return mixed
     */
    private function callApiProducts( $request ){

        sleep(1);
        $response = Http::withHeaders([
            'X-Gateway-APIKey' => $this->apiKey,
        ])->get('https://api-gateway.leroymerlin.fr/api-product/v2' . $request);

        return $response->json();
    }
}

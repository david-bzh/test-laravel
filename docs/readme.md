# Informations sur le développement

## Ressource utilisée

* Docs: [Laravel](https://laravel.com/docs/8.x) 
* Docs: [Jetstream](https://jetstream.laravel.com/2.x/introduction.html) 
* Docs: [Livewire](https://laravel-livewire.com/docs/2.x/quickstart) 
* Docs: [API LeroyMerlin](https://developer.leroymerlin.fr/docs/api/405d0ee3-1879-4fd3-9d36-50455b27e23f) 
* Youtube fr [ Formation Laravel 8 - 1/26  ](https://www.youtube.com/watch?v=SbR0-dGg3tE&list=PLlxQJeQRaKDRIaejG52-KjLnTIlyi0mxt)
* Youtube fr [ Tutoriel Laravel Livewire - 1 ](https://www.youtube.com/watch?v=64wRns-TvQ8&list=PLlxQJeQRaKDTIj68ASaMwacCC72UKtiLA)
* Youtube autre vidéo non fr
* [grepper](https://www.codegrepper.com/) affiche le resultat de la cherche directement sur page de chrome
* Ide Vscode
* Postman
